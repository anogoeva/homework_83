const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const exitHook = require('async-exit-hook');
const artists = require('./app/artists');
const albums = require('./app/albums');
const users = require('./app/users');
const tracks = require('./app/tracks');
const trackHistory = require('./app/trackHistory');


const app = express();
app.use(express.json());
app.use(cors());
app.use(express.static('public'));

const port = 8000;

app.use('/artists', artists);
app.use('/albums', albums);
app.use('/tracks', tracks);
app.use('/users', users);
app.use('/track_history', trackHistory);

const run = async () => {
  await mongoose.connect('mongodb://localhost/music');

  app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
  });

  exitHook(() => {
    console.log('Successfully disconnected from the DB');
    mongoose.disconnect();
  });
};

run().catch(e => console.error(e));