const express = require('express');
const User = require("../models/User");
const dayjs = require("dayjs");
const TrackHistory = require("../models/TrackHistory");
const router = express.Router();


router.post('/', async (req, res) => {
  const token = req.get('Authorization');

  if (!token) {
    return res.status(401).send({error: 'No token present'});
  }

  const user = await User.findOne({token});

  if (!user) {
    return res.status(401).send({error: 'Wrong token'});
  }

  const trackHistoryData = {
    user: user._id,
    track: req.body.track,
    datetime: dayjs().format()
  };

  const trackHistory = new TrackHistory(trackHistoryData);
  try {
    await trackHistory.save();
    res.send(trackHistory);
  } catch (error) {
    res.status(500).send(error);
  }
});


router.get('/', async (req, res) => {
  try {
    const trackHistoriesData = await TrackHistory.find().populate('user', 'username').populate('track', 'title');
    res.send(trackHistoriesData);
  } catch (e) {
    res.sendStatus(500);
  }
});

module.exports = router;