const express = require('express');
const Artist = require('../models/Artist');
const path = require("path");
const multer = require('multer');
const config = require('../config');
const {nanoid} = require('nanoid');
const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  try {
    const artists = await Artist.find();
    res.send(artists);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', upload.single('image'), async (req, res) => {
  if (!req.body.title) {
    return res.status(400).send('Data not valid');
  }

  const artistData = {
    title: req.body.title,
    information: req.body.information || null,
  };

  if (req.file) {
    artistData.photo = req.file.filename;
  } else {
    artistData.photo = null;
  }

  const artist = new Artist(artistData);

  try {
    await artist.save();
    res.send(artist);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;