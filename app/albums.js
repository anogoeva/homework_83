const express = require('express');
const Album = require('../models/Album');
const multer = require("multer");
const config = require("../config");
const {nanoid} = require("nanoid");
const path = require("path");

const router = express.Router();

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

router.get('/', async (req, res) => {
  try {
    const query = {};
    if (req.query.artist) {
      query.artist = req.query.artist;
    }
    const albums = await Album.find(query).populate('artist', 'title');
    res.send(albums);
  } catch (e) {
    res.sendStatus(500);
  }
});

router.post('/', upload.single('image'), async (req, res) => {
  if (!req.body.title || !req.body.artist) {
    return res.status(400).send('Data not valid');
  }

  const albumData = {
    title: req.body.title,
    artist: req.body.artist,
    year: req.body.year || null,
  };

  if (req.file) {
    albumData.image = req.file.filename;
  } else {
    albumData.image = null;
  }

  const album = new Album(albumData);

  try {
    await album.save();
    res.send(album);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.get('/:id', async (req, res) => {
  try {
    const album = await Album.findById(req.params.id).populate('artist', 'title');

    if (album) {
      res.send(album);
    } else {
      res.status(404).send({error: 'Album not found'});
    }
  } catch {
    res.sendStatus(500);
  }
});

module.exports = router;